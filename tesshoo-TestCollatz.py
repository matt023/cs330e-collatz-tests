#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "100 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 1000)

    def test_read_4(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 10)
        self.assertEqual(j, 1)
    
    def test_read_5(self):
        s = "800 700\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 800)
        self.assertEqual(j, 700)

    def test_read_6(self):
        s = "729387 745959\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 729387)
        self.assertEqual(j, 745959)


    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(50, 50)
        self.assertEqual(v, 25)
    
    def test_eval_6(self):
        v = collatz_eval(888, 888)
        self.assertEqual(v, 73)

    def test_eval_7(self):
        v = collatz_eval(777, 555)
        self.assertEqual(v, 171)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 30, 1, 112)
        self.assertEqual(w.getvalue(), "30 1 112\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 100, 90, 119)
        self.assertEqual(w.getvalue(), "100 90 119\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 80, 80, 10)
        self.assertEqual(w.getvalue(), "80 80 10\n")
    
    def test_print_6(self):
        w = StringIO()
        collatz_print(w, 255, 255, 48)
        self.assertEqual(w.getvalue(), "255 255 48\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        
    def test_solve_2(self):
        r = StringIO("1 30\n90 100\n301 310\n800 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 30 112\n90 100 119\n301 310 87\n800 1000 179\n"
        )
    
    def test_solve_3(self):
        r = StringIO("8000 8001\n200 80\n999998 999999\n900 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "8000 8001 115\n200 80 125\n999998 999999 259\n900 900 55\n"
        )

    def test_solve_4(self):
        r = StringIO("202 188\n893 893\n410 359\n7843 7843\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "202 188 120\n893 893 73\n410 359 121\n7843 7843 53\n"
        )
        
    def test_solve_5(self):
        r = StringIO("102559 102784\n938493 938493\n19384 19000\n3923 3923\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "102559 102784 310\n938493 938493 158\n19384 19000 212\n3923 3923 176\n"
        )
    
    def test_solve_6(self):
        r = StringIO("3948 81943\n8384 34\n5000 7893\n940 8394")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "3948 81943 351\n8384 34 262\n5000 7893 262\n940 8394 262\n"
        )

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
