#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read2(self):
        s = "109 325\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 109)
        self.assertEqual(j, 325)

    def test_read3(self):
        s = "999999 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999999)
        self.assertEqual(j, 1)

    def test_read4(self):
        s = "765 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 765)
        self.assertEqual(j, 2)
        

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(999999, 1)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(10000, 10000)
        self.assertEqual(v, 30)
        
    def test_eval_8(self):
        v = collatz_eval(2, 27)
        self.assertEqual(v, 112)
        
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print2(self):
        w = StringIO()
        collatz_print(w, 90, 9000, 999999)
        self.assertEqual(w.getvalue(), "90 9000 999999\n")

    def test_print3(self):
        w = StringIO()
        collatz_print(w, 'hello', 'hi', 'bye')
        self.assertEqual(w.getvalue(), "hello hi bye\n")

    def test_print4(self):
        w = StringIO()
        collatz_print(w, 90.789, 'no', 80)
        self.assertEqual(w.getvalue(), "90.789 no 80\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve2(self):
        r = StringIO("1 1\n10000 10000\n999999 1\n2 888\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n10000 10000 30\n999999 1 525\n2 888 179\n")

    def test_solve3(self):
        r = StringIO("999999 999999\n1 999999\n500 500\n7 7\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 999999 259\n1 999999 525\n500 500 111\n7 7 17\n")

    def test_solve4(self):
        r = StringIO("45 54\n33 333\n100 9999\n77 77\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "45 54 113\n33 333 144\n100 9999 262\n77 77 23\n")
        
        

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
